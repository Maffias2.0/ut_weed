function createPlant(data)
  local self = {}

  self.x = data.coords.x
  self.y = data.coords.y
  self.z = data.coords.z
  self.strain = data.strain
  self.type = data.type
  self.stage = (data.stage == nil and 1 or data.stage)
  self.isInHouse = data.isInHouse
  self.isInMotel = data.isInMotel
  self.isInBuilding = (data.isInMotel or data.isInHouse)
  self.timeModifier = 1
  if self.isInBuilding then
    if data.isInMotel then
      self.timeModifier = config.multipliers.motel
    elseif data.isInHouse then
      self.timeModifier = config.multipliers.house
    end
  end
  self.isDone = (data.isDone == nil and false or data.isDone)

  self.isBeingHarvested = false
  self.isBeingDestroyed = false

  self.quantity = data.quantity

  self.progress = (data.progress ~= nil and data.progress or 0)
  self.timeLeft = (self.progress == 0 and config.defaultProcessTime or ((100-self.progress)/100)*config.defaultProcessTime)
  self.quality = (data.quality ~= nil and data.quality or 0)
  self.water = (data.water ~= nil and data.water or 0)
  self.fertilizer = (data.fertilizer ~= nil and data.fertilizer or 0)

  if data.id == nil then
    while true do
      Citizen.Wait(0)
      self.id = math.random(1, 10000000)
      if plants[self.id] == nil and plantsDone[self.id] == nil then
        break
      end
    end
  else
    self.id = data.id
  end

  self.secPassed = function()
    Citizen.CreateThread(function()
      local oldProgress = self.progress
      local oldWater = getQuality(self.water)
      local oldFertilizer = getQuality(self.fertilizer)
      local oldQuality = getQuality(self.quality)
      local oldStage = self.stage
      if not self.isDone then
        self.timeLeft = self.timeLeft-(1000*(self.timeModifier)*((self.quality*config.qualityModifier)+1))
        self.water = (self.water - config.modifier < 0 and 0 or self.water-config.modifier)
        self.fertilizer = (self.fertilizer - config.modifier < 0 and 0 or self.fertilizer-config.modifier)
        local lowest = (self.water > self.fertilizer and self.fertilizer or self.water)
        self.quality = (self.quality > 1 and 1 or (self.quality > lowest and self.quality - ((1-lowest)*config.modifier*100) or self.quality + (lowest*config.modifier*100)))
        if self.timeLeft <= 0 then
          self.isDone = true
          self.progress = 100
          self.stage = 4
        else
          self.progress = (1-(self.timeLeft/config.defaultProcessTime))*100
          self.stage = (self.progress < 33 and 1 or (self.progress < 66 and 2 or 3))
        end
        if math.floor(oldProgress) < math.floor(self.progress) or oldWater ~= getQuality(self.water) or oldFertilizer ~= getQuality(self.fertilizer) or oldQuality ~= getQuality(self.quality) then
          if plantsBeingInteractedWith[self.id] ~= nil then
            for k,v in pairs(plantsBeingInteractedWith[self.id]) do
              TriggerClientEvent('ut_weed:updatePlant', k, self.id)
            end
          end
        end
        if oldStage ~= self.stage then
          TriggerClientEvent('ut_weed:updatePlantObj', -1, self)
        end
      else
        local yield = getQualityIndex(self.quality)
        self.quantity = (self.quantity == nil and math.random(config.yield[self.strain][yield].min, config.yield[self.strain][yield].max) or self.quantity)
        TriggerEvent('ut_weed:changePlantIsDone', self.id)
      end
    end)
  end

  return self
end
