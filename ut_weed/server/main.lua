RegisterServerEvent('ut_weed:addInteracting')
RegisterServerEvent('ut_weed:createNewPlant')
RegisterServerEvent('ut_weed:removeInteracting')
RegisterServerEvent('ut_weed:changePlantIsDone')
RegisterServerEvent('ut_weed:harvesting')
RegisterServerEvent('ut_weed:stoppedHarvesting')
RegisterServerEvent('ut_weed:save')
RegisterServerEvent('ut_weed:startDestroying')
RegisterServerEvent('ut_weed:doneDestroying')
RegisterServerEvent('ut_weed:fillItemServer')

local ESX = nil
plants = {}
plantsDone = {}
plantsBeingInteractedWith = {}

Citizen.CreateThread(function()
  while ESX == nil do
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    Citizen.Wait(0)
  end

  ESX.RegisterServerCallback('ut_weed:hasItemsRequired', function(source, cb, seed)
    local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

    if xPlayer.getInventoryItem(seed) ~= nil and xPlayer.getInventoryItem("plantpot") ~= nil then
      xPlayer.removeInventoryItem(seed, 1)
      xPlayer.removeInventoryItem('plantpot', 1)
      cb(true)
    else
      cb(false)
    end
  end)

  ESX.RegisterServerCallback('ut_weed:getPlant', function(source, cb, id)
    cb((plants[id] == nil and plantsDone[id] or plants[id]))
  end)

  ESX.RegisterServerCallback('ut_weed:getAllPlants', function(source, cb)
    cb(plants, plantsDone)
  end)

  Wait(2500)
  local result = MySQL.Sync.fetchAll('SELECT * FROM ut_weed', {})
  for k,v in pairs(result) do
    local v = json.decode(v.plant)
    if v.isDone == true then
      plantsDone[v.id] = createPlant(v)
      TriggerClientEvent('ut_weed:spawnPlant', -1, plantsDone[v.id])
    else
      plants[v.id] = createPlant(v)
      TriggerClientEvent('ut_weed:spawnPlant', -1, plants[v.id])
    end
  end
end)

-- MySQL.ready(function()
  -- local result = MySQL.Sync.fetchAll('SELECT * FROM ut_weed', {})
  -- for k,v in pairs(result) do
  --   local v = json.decode(v.plant)
  --   TriggerEvent('ut_weed:createNewPlant', v.coords.x, v.coords.y, v.coords.z, v.stage, v.strain, v.isInMotel, v.isInHouse, v.type)
  -- end
-- end)

AddEventHandler('ut_weed:createNewPlant', function(x, y, z, stage, strain, isInMotel, isInHouse, type)
  local _source = source
  local temp = createPlant({coords = {x = x, y = y, z = z}, stage = stage, strain = strain, isInMotel = isInMotel, isInHouse = isInHouse, type = type})
  plants[temp.id] = temp
  if isInHouse and isInMotel then
    TriggerClientEvent("ut_weed:spawnPlant", _source, id)
    return
  end
  TriggerClientEvent("ut_weed:spawnPlant", -1, temp)

  MySQL.Sync.execute('INSERT INTO ut_weed VALUES(@id, @data)', {
    ["@id"] = temp.id,
    ["@data"] = json.encode({
      coords = {
        x = temp.x,
        y = temp.y,
        z = temp.z,
      },
      strain = temp.strain,
      type = temp.type,
      stage = temp.stage,
      isInHouse = temp.isInHouse,
      isInMotel = temp.isInMotel,
      id = temp.id
    })
  })
end)

Citizen.CreateThread(function()
  local save = config.saveInterval
  while true do
    Citizen.Wait(1000)
    for k,v in pairs(plants) do
      plants[k].secPassed()
    end
    save = save-1000
    if save <= 0 then
      TriggerEvent('ut_weed:save')
      save = config.saveInterval
    end
  end
end)

AddEventHandler('ut_weed:addInteracting', function(id)
  local _source = source
  if plantsBeingInteractedWith[id] == nil then
    plantsBeingInteractedWith[id] = {}
  end
  plantsBeingInteractedWith[id][_source] = true
end)

AddEventHandler('ut_weed:removeInteracting', function(id)
  local _source = source
  plantsBeingInteractedWith[id][_source] = nil
  for k,v in pairs(plantsBeingInteractedWith[id]) do
    return
  end
  plantsBeingInteractedWith[id] = nil
end)

function getQuality(percent)
  local temp = 1
  for k,v in pairs(config.quality) do
    if percent >= v.quality then
      temp = k
    else
      return config.quality[temp].quality
    end
  end
  return config.quality[6].quality
end

function getQualityIndex(percent)
  local temp = 1
  for k,v in pairs(config.quality) do
    if percent >= v.quality then
      temp = k
    else
      return temp
    end
  end
  return 6
end

AddEventHandler('ut_weed:changePlantIsDone', function(plantId)
  local plant = plants[plantId]
  plants[plantId] = nil
  plantsDone[plantId] = plant
  if plantsBeingInteractedWith[plantId] ~= nil then
    for k,v in pairs(plantsBeingInteractedWith[plantId]) do
      TriggerClientEvent('ut_weed:updatePlant', k, plantId)
    end
  end
end)

AddEventHandler('ut_weed:harvesting', function(id)
  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)

  if plantsDone[id].isBeingHarvested then
    TriggerClientEvent("mythic_notify:client:SendAlert", _source, {
      text = "Plant is already being harvested",
      type = 'error',
    })
    return
  elseif not xPlayer.canCarry("trimmedweed", plantsDone[id].quantity) then
    TriggerClientEvent("mythic_notify:client:SendAlert", _source, {
      text = "Trimmings too heavy",
      type = 'error',
    })
    return
  elseif xPlayer.isAboveLimit("trimmedweed", plantsDone[id].quantity) then
    TriggerClientEvent("mythic_notify:client:SendAlert", _source, {
      text = "Trimmings limit reached",
      type = 'error',
    })
    return
  end

  TriggerClientEvent("ut_weed:harvest", _source, id)

  plantsDone[id].isBeingHarvested = true
end)

AddEventHandler('ut_weed:stoppedHarvesting', function(cancelled, id)
  local _source = source
  plantsDone[id].isBeingHarvested = false

  if not cancelled then
    local xPlayer = ESX.GetPlayerFromId(_source)

    if not xPlayer.canCarry("trimmedweed", plantsDone[id].quantity) then
      TriggerClientEvent("mythic_notify:client:SendAlert", _source, {
        text = (plantsDone[id].type == "Female" and "Trimmings too heavy" or "Seeds too heavy"),
        type = 'error',
      })
      return
    elseif xPlayer.isAboveLimit("trimmedweed", plantsDone[id].quantity) then
      TriggerClientEvent("mythic_notify:client:SendAlert", _source, {
        text = (plantsDone[id].type == "Female" and "Seed limit reached" or "Trimmings limit reached"),
        type = 'error',
      })
      return
    end

    xPlayer.addInventoryItem((plantsDone[id].type == "Female" and "trimmedweed" or (plantsDone[id].strain == "highgrademaleseed" and "highgradefemaleseed" or "lowgradefemaleseed")), plantsDone[id].quantity)
    plantsDone[id] = nil
    if plantsBeingInteractedWith[id] ~= nil then
      for k,v in pairs(plantsBeingInteractedWith[id]) do
        TriggerClientEvent('ut_weed:updatePlant', k, id)
      end
    end
    TriggerClientEvent('ut_weed:removePlant', -1, id)
    MySQL.Sync.execute("DELETE FROM ut_weed WHERE id = @id", {
      ["@id"] = id
    })
  end
end)

RegisterServerEvent('ut_weed:breakDownWeed')
AddEventHandler('ut_weed:breakDownWeed', function(_source)
  local xPlayer = ESX.GetPlayerFromId(_source)

  xPlayer.removeInventoryItem('trimmedweed', 1)
  xPlayer.addInventoryItem("weedbag", 1)
end)

AddEventHandler('ut_weed:save', function()
  for k,v in pairs(plants) do
    MySQL.Sync.execute("UPDATE ut_weed SET plant = @data WHERE id = @id", {
      ["@id"] = k,
      ["@data"] = json.encode({
        coords = {
          x = v.x,
          y = v.y,
          z = v.z,
        },
        strain = v.strain,
        type = v.type,
        stage = v.stage,
        isInHouse = v.isInHouse,
        isInMotel = v.isInMotel,
        id = v.id,
        quantity = v.quantity,
        progress = v.progress,
        quality = v.quality,
        water = v.water,
        fertilizer = v.fertilizer
      })
    })
  end
  for k,v in pairs(plantsDone) do
    MySQL.Sync.execute("UPDATE ut_weed SET plant = @data WHERE id = @id", {
      ["@id"] = k,
      ["@data"] = json.encode({
        coords = {
          x = v.x,
          y = v.y,
          z = v.z,
        },
        strain = v.strain,
        type = v.type,
        stage = v.stage,
        isInHouse = v.isInHouse,
        isInMotel = v.isInMotel,
        id = v.id,
        quantity = v.quantity,
        progress = v.progress,
        quality = v.quality,
        water = v.water,
        fertilizer = v.fertilizer,
        isDone = true
      })
    })
  end
end)

AddEventHandler('ut_weed:startDestroying', function(id)
  local _source = source

  if plants[id] ~= nil then
    if plants[id].isBeingDestroyed then
      TriggerClientEvent("mythic_notify:client:SendAlert", _source, {
        text = "Plant is already being destroyed",
        type = 'error',
      })
      return
    end
  else
    if plantsDone[id].isBeingDestroyed then
      TriggerClientEvent("mythic_notify:client:SendAlert", _source, {
        text = "Plant is already being destroyed",
        type = 'error',
      })
      return
    end
  end
  TriggerClientEvent('ut_weed:destroy', _source, id)
end)

AddEventHandler('ut_weed:doneDestroying', function(cancelled, id)
  if cancelled then
    return
  end
  plantsDone[id] = nil
  plants[id] = nil
  if plantsBeingInteractedWith[id] ~= nil then
    for k,v in pairs(plantsBeingInteractedWith[id]) do
      TriggerClientEvent('ut_weed:updatePlant', k, id)
    end
  end
  TriggerClientEvent('ut_weed:removePlant', -1, id)
  MySQL.Sync.execute("DELETE FROM ut_weed WHERE id = @id", {
    ["@id"] = id
  })
end)

AddEventHandler('ut_weed:fillItemServer', function(plant, quality, type)
  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)

  if plant == nil then
    TriggerClientEvent("mythic_notify:client:SendAlert", _source, {
      text = "Not close enough to plant",
      type = 'error',
    })
    return
  elseif xPlayer.getInventoryItem(quality, 1) == nil then
    TriggerClientEvent("mythic_notify:client:SendAlert", _source, {
      text = "You do not have this item",
      type = 'error',
    })
    return
  end

  xPlayer.removeInventoryItem(quality, 1)

  plants[plant.id][type] = (plants[plant.id][type]+config.adding[quality].adding > 1 and 1 or plants[plant.id][type]+config.adding[quality].adding)

end)
