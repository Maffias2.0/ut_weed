resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

ui_page "html/main.html"

client_scripts {
	"config.lua",
	"client/main.lua"
}

server_scripts {
	'@mysql-async/lib/MySQL.lua',
	"config.lua",
	"server/main.lua",
	"server/object.lua"
}

files {
	"html/main.html",
	"html/main.css",
	"html/main.js",
}
