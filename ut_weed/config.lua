config = {}

config.objects = {
  1511282135, --1027382312, --1315651205, --1511282135
  1027382312,
  870605061,
  716763602
}

config.multipliers = {
  motel = 0.25,
  house = 0.5
}

config.defaultProcessTime = 600000 -- Start time when placed

config.saveInterval = 60000 -- Interval of which information saves to database

config.harvestTime = 5000 -- Time it takes to harvest

config.qualityModifier = 0.005 -- Do *100 to find time modifier at 100% quality timeLeft = timeLeft/0.5

config.modifier = 0.0005 -- Do *100 for the potential growth to quality per second

config.waterDegration = 0.0005 -- How much water gets removed per second
config.fertilizerDegration = 0.0005 -- How much fertilizer gets removed every second

--[[
  With current config data it takes 10 highgrade to get to full quality over time
  This will degrade back to 0 in 33 minutes which will keep it at medium quality
]]

config.adding = {
  highgradefert = {
    adding = 0.1, -- This value / waterDegration is how many seconds it will last until it goes to previous
    type = "fertilizer"
  },
  lowgradefert = {
    adding = 0.05,-- This value / waterDegration is how many seconds it will last until it goes to previous
    type = "fertilizer"
  },
  purifiedwater = {
    adding = 0.1,
    type = "water"
  },
  wateringcan = {
    adding = 0.05,
    type = "water"
  },
}

config.quality = {
  {
    quality = 0,
    color = "~r~",
  },
  {
    quality = 0.20,
    color = "~o~",
  },
  {
    quality = 0.40,
    color = "~y~",
  },
  {
    quality = 0.60,
    color = "~g~",
  },
  {
    quality = 0.80,
    color = "~b~",
  },
  {
    quality = 0.95,
    color = "~p~",
  },
}

config.yield = {
  highgradefemaleseed = {
    {
      min = 50,
      max = 100
    },
    {
      min = 100,
      max = 150
    },
    {
      min = 150,
      max = 200
    },
    {
      min = 200,
      max = 250
    },
    {
      min = 250,
      max = 300
    },
    {
      min = 300,
      max = 350
    },
  },
  lowgradefemaleseed = {
    {
      min = 25,
      max = 50
    },
    {
      min = 50,
      max = 75
    },
    {
      min = 75,
      max = 100
    },
    {
      min = 100,
      max = 125
    },
    {
      min = 125,
      max = 150
    },
    {
      min = 150,
      max = 175
    },
  },
  highgrademaleseed = {
    {
      min = 5,
      max = 10
    },
    {
      min = 10,
      max = 15
    },
    {
      min = 15,
      max = 20
    },
    {
      min = 20,
      max = 25
    },
    {
      min = 25,
      max = 30
    },
    {
      min = 30,
      max = 35
    },
  },
  lowgrademaleseed = {
    {
      min = 5,
      max = 10
    },
    {
      min = 10,
      max = 15
    },
    {
      min = 15,
      max = 20
    },
    {
      min = 20,
      max = 25
    },
    {
      min = 25,
      max = 30
    },
    {
      min = 30,
      max = 35
    },
  }
}
