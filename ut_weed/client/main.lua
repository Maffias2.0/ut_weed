local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

RegisterNetEvent('ut_motel:enterRoom')
RegisterNetEvent('ut_motel:door')
RegisterNetEvent('ut_realestate:enterHouse')
RegisterNetEvent('ut_realestate:leaveHouse')
RegisterNetEvent('ut_realestate:enterGarage')
RegisterNetEvent('ut_realestate:leaveGarage')
RegisterNetEvent('ut_garages:enterGarage')
RegisterNetEvent('ut_garages:leaveGarage')
RegisterNetEvent('ut_weed:updatePlant')
RegisterNetEvent('ut_weed:spawnPlant')
RegisterNetEvent('ut_weed:harvest')
RegisterNetEvent('ut_weed:updatePlantObj')
RegisterNetEvent('ut_weed:removePlant')
RegisterNetEvent('esx:setJob')
RegisterNetEvent('ut_weed:destroy')
RegisterNetEvent('ut_weed:fillItem')

local ESX = nil
local interactions = {}
local refreshInteractions = false
local activeInteractions = {}
local isInMotel = false
local isInHouse = false
local isInGarage = false
local plants = {}
local isHarvesting = nil
local isDestroying = nil
local job = {}

Citizen.CreateThread(function()
  while ESX == nil or ESX.PlayerLoaded == false do
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    Citizen.Wait(0)
  end

  ESX.TriggerServerCallback('ut_weed:getAllPlants', function(plant, plantsDone)
    for k,v in pairs(plant) do
      plants[v.id] = CreateObject(config.objects[v.stage], v.x, v.y, v.z + getPlantZnotOffset(config.objects[v.stage]), false, false, true)
      FreezeEntityPosition(plants[v.id], true)
      table.insert(interactions, {
        text = formatText(v),
        action = nil,
        pos = {
          x = v.x,
          y = v.y,
          z = v.z-0.5
        },
        dist = 5,
        canInteractWithCar = false,
        background = false,
        id = v.id
      })
    end
    for k,v in pairs(plantsDone) do
      plants[v.id] = CreateObject(config.objects[v.stage], v.x, v.y, v.z + getPlantZnotOffset(config.objects[v.stage]), false, false, true)
      FreezeEntityPosition(plants[v.id], true)
      table.insert(interactions, {
        text = formatText(v),
        action = nil,
        pos = {
          x = v.x,
          y = v.y,
          z = v.z-0.5
        },
        dist = 5,
        canInteractWithCar = false,
        background = false,
        id = v.id
      })
    end
  end)

  job = ESX.GetPlayerData().job
end)

AddEventHandler('esx:setJob', function(x)
  job = x
  for k,v in pairs(interactions) do
    TriggerEvent("ut_weed:updatePlant", v.id)
  end
  refreshInteractions = true
end)


AddEventHandler('ut_motel:enterRoom', function()
  isInMotel = true
end)

AddEventHandler('ut_motel:door', function()
  isInMotel = false
end)

AddEventHandler('ut_realestate:enterHouse', function()
  isInHouse = true
end)

AddEventHandler('ut_realestate:leaveHouse', function()
  isInHouse = false
end)

AddEventHandler('ut_realestate:enterGarage', function()
  isInGarage = true
end)

AddEventHandler('ut_realestate:leaveGarage', function()
  isInGarage = false
end)

AddEventHandler('ut_garages:enterGarage', function()
  isInGarage = true
end)

AddEventHandler('ut_garages:leaveGarage', function()
  isInGarage = false
end)

function getDistance(coords1, coords2)
  return Vdist2(coords1.x, coords1.y, coords1.z, coords2.x, coords2.y, coords2.z)
end

function Draw3DText(x,y,z, text, background)
  local onScreen,_x,_y=World3dToScreen2d(x,y,z)
  local px,py,pz=table.unpack(GetGameplayCamCoords())

  SetTextScale(0.4, 0.4)
  SetTextFont(4)
  SetTextProportional(1)
  SetTextColour(255, 255, 255, 215)
  SetTextEntry("STRING")
  SetTextCentre(1)
  AddTextComponentString(text)
  DrawText(_x,_y)
  if background ~= false then
    local factor = (string.len(text)) / 370
    DrawRect(_x,_y+0.0125, 0.02+ factor, 0.05, 110, 110, 110, 75)
  end
end


Citizen.CreateThread(function()
  while true do
    Wait(500)
    local ped = GetPlayerPed(-1)
    local coords = GetEntityCoords(ped)
    for k,v in pairs(interactions) do
      if activeInteractions[v.id] == nil and getDistance(coords, v.pos) < v.dist and interactions[k] ~= nil then
        activeInteractions[v.id] = true
        closeToInteraction(k, v)
      end
    end
  end
end)

function closeToInteraction(k, v)
  Citizen.CreateThread(function()
    local ped = GetPlayerPed(-1)
    local coords = GetEntityCoords(ped)
    ESX.TriggerServerCallback('ut_weed:getPlant', function(plant)
      if plant ~= nil then
        if job.name == "police" then
          interactions[k].action = function()
            TriggerServerEvent('ut_weed:startDestroying', plant.id)
          end
        else
          if plant.isDone then
            interactions[k].action = function()
              TriggerServerEvent('ut_weed:harvesting', plant.id)
            end
          end
        end
        interactions[k].text = formatText(plant)
      else
        interactions[k] = nil
        refreshInteractions = true
      end
      v.text = formatText(plant)
    end, v.id)
    TriggerServerEvent('ut_weed:addInteracting', v.id)
    while getDistance(coords, v.pos) < v.dist and not refreshInteractions do
      Wait(0)
      ped = GetPlayerPed(-1)
      coords = GetEntityCoords(ped)
      if getDistance(coords, v.pos) < v.dist and (v.canInteractWithCar or (not v.canInteractWithCar and (not (GetVehiclePedIsIn(ped, false) ~= 0)))) then
        Draw3DText(v.pos.x, v.pos.y, v.pos.z+1.5, v.text, v.background)
        if IsControlPressed(0, Keys["E"]) and v.action ~= nil then
          v.action()
          Wait(500)
        end
      end
    end
    TriggerServerEvent('ut_weed:removeInteracting', v.id)
    if isHarvesting ~= nil or isDestroying ~= nil then
      TriggerEvent("mythic_progbar:client:cancel")
    end
    refreshInteractions = false
    activeInteractions[v.id] = nil
  end)
end

RegisterNetEvent('ut_weed:plantSeed')
AddEventHandler('ut_weed:plantSeed', function(strain, type)
  if isInGarage then
    TriggerEvent("mythic_notify:client:SendAlert", {
      text = "Cannot plant in a garage",
      type = 'error',
    })
    return
  elseif isInMotel then
    TriggerEvent("mythic_notify:client:SendAlert", {
      text = "Cannot plant in a motel",
      type = 'error',
    })
    return
  elseif isInHouse then
    TriggerEvent("mythic_notify:client:SendAlert", {
      text = "Cannot plant in a house",
      type = 'error',
    })
    return
  end
  ESX.TriggerServerCallback('ut_weed:hasItemsRequired', function(hasItems)
    if hasItems then
      placing(strain, type)
    else
      TriggerEvent("mythic_notify:client:SendAlert", {
        text = "You do not have the required items",
        type = 'error',
      })
    end
  end, strain)
end)

function placing(strain, type)
  Citizen.CreateThread(function()
    local ped = GetPlayerPed(-1)
    local y = 1.0
    local x = 0.0
    local z = 0.0
    local coordsOffset = GetOffsetFromEntityInWorldCoords(ped, x, y, getPlantZ(config.objects[1])+z)
    local plant = CreateObject(config.objects[1], coordsOffset.x, coordsOffset.y, coordsOffset.z, false, false, true)
    SendNUIMessage({
      enable = true,
    })
    local placed = false
    while not placed do
      Wait(0)
      if IsControlPressed(0, 172) and y < 4 then
        y = y+0.1
        Wait(50)
      elseif IsControlPressed(0, 173) and y > 1 then
        y = y-0.1
        Wait(50)
      elseif IsControlPressed(0, 174) and x > -4 then
        x = x-0.1
        Wait(50)
      elseif IsControlPressed(0, 175) and x < 4 then
        x = x+0.1
        Wait(50)
      elseif IsControlPressed(0, 208) and z < 1 then
        z = z+0.05
        Wait(50)
      elseif IsControlPressed(0, 207) and z > -1 then
        z = z-0.05
        Wait(50)
      elseif IsControlJustReleased(0, 179) then
        DeleteObject(plant)
        placed = true
        SendNUIMessage({
          enable = false,
        })
        TriggerServerEvent('ut_weed:createNewPlant', coordsOffset.x, coordsOffset.y, coordsOffset.z, 1, strain, isInMotel, isInHouse, type)
        return
      end
      DeleteObject(plant)
      coordsOffset = GetOffsetFromEntityInWorldCoords(ped, x, y, getPlantZ(config.objects[1])+z)
      plant = CreateObject(config.objects[1], coordsOffset.x, coordsOffset.y, coordsOffset.z, false, false, true)
    end
  end)
end

function getPlantZ(plant)
  if plant == 870605061 then
    return -3.5
  else
    return -1.0
  end
end

function getPlantZnotOffset(plant)
  if plant == 870605061 or plant == 716763602 then
    return -2.5
  else
    return 0.0
  end
end

AddEventHandler('ut_weed:updatePlant', function(id)
  for k,v in pairs(interactions) do
    if v.id == id then
      ESX.TriggerServerCallback('ut_weed:getPlant', function(plant)
        if plant ~= nil then
          if job.name == "police" then
            interactions[k].action = function()
              TriggerServerEvent('ut_weed:startDestroying', plant.id)
            end
          else
            if plant.isDone then
              interactions[k].action = function()
                TriggerServerEvent('ut_weed:harvesting', plant.id)
              end
            end
          end
          interactions[k].text = formatText(plant)
        else
          interactions[k] = nil
          refreshInteractions = true
        end
      end, id)
      return
    end
  end
end)

AddEventHandler('ut_weed:spawnPlant', function(plant)
  plants[plant.id] = CreateObject(config.objects[plant.stage], plant.x, plant.y, plant.z + getPlantZnotOffset(config.objects[plant.stage]), false, true, true)
  FreezeEntityPosition(plants[plant.id], true)
  table.insert(interactions, {
    text = formatText(plant),
    action = nil,
    pos = {
      x = plant.x,
      y = plant.y,
      z = plant.z-0.5
    },
    dist = 5,
    canInteractWithCar = false,
    background = false,
    id = plant.id
  })
  if job.name == "police" then
    for k,v in pairs(interactions) do
      if v.id == plant.id then
        interactions[k].action = function()
          TriggerServerEvent('ut_weed:startDestroying', plant.id)
        end
      end
    end
  elseif plant.isDone then
    for k,v in pairs(interactions) do
      if v.id == plant.id then
        interactions[k].action = function()
          TriggerServerEvent('ut_weed:harvesting', plant.id)
        end
      end
    end
  end
end)

function formatText(plant)
  if job.name ~= "police" then
    if not plant.isDone then
      return "[ " .. plant.type .. " ] [ " .. math.floor(plant.progress) .. "% Growth ] [ " .. getColor(plant.quality) .. "Quality~s~ ]\n[ " .. getColor(plant.fertilizer) .. "Fertilizer~s~ ] [ " .. getColor(plant.water) .. "Water~s~ ]"
    else
      return "[ " .. plant.type .. " ] [ Press E to harvest ] [ " .. getColor(plant.quality) .. "Quality~s~ ]"
    end
  else
    return "Press E to destory weed"
  end
end

function getColor(percent)
  local temp = 1
  for k,v in pairs(config.quality) do
    if percent >= v.quality then
      temp = k
    else
      return config.quality[temp].color
    end
  end
  return config.quality[6].color
end

AddEventHandler('ut_weed:harvest', function(id)
  isHarvesting = id
  local ped = GetPlayerPed(-1)
  TaskTurnPedToFaceEntity(ped, plants[id], -1)
  Citizen.Wait(1000)
  TaskStartScenarioInPlace(ped, "PROP_HUMAN_BUM_BIN", 0, true)
  TriggerEvent("mythic_progbar:client:progress", {
    name = "harvesting",
    duration = config.harvestTime,
    label = "Harvesting weed",
    useWhileDead = false,
    canCancel = true
  }, function(cancelled)
    isHarvesting = nil
    ClearPedTasksImmediately(ped)
    TriggerServerEvent('ut_weed:stoppedHarvesting', cancelled, id)
  end)
end)

AddEventHandler('ut_weed:destroy', function(id)
  isDestroying = id
  local ped = GetPlayerPed(-1)
  TaskTurnPedToFaceEntity(ped, plants[id], -1)
  Citizen.Wait(1000)
  TaskStartScenarioInPlace(ped, "PROP_HUMAN_BUM_BIN", 0, true)
  TriggerEvent("mythic_progbar:client:progress", {
    name = "harvesting",
    duration = config.harvestTime,
    label = "Harvesting weed",
    useWhileDead = false,
    canCancel = true
  }, function(cancelled)
    isHarvesting = nil
    ClearPedTasksImmediately(ped)
    TriggerServerEvent('ut_weed:stoppedHarvesting', cancelled, id)
  end)
end)

AddEventHandler('ut_weed:updatePlantObj', function(plant)
  if plants[plant.id] ~= nil then
    DeleteObject(plants[plant.id])
    plants[plant.id] = CreateObject(config.objects[plant.stage], plant.x, plant.y, plant.z + getPlantZnotOffset(config.objects[plant.stage]), false, true, true)
    FreezeEntityPosition(plants[plant.id], true)
  end
end)

AddEventHandler('ut_weed:removePlant', function(id)
  DeleteObject(plants[id])
end)

AddEventHandler('ut_weed:destroy', function(id)
  isDestroying = id
  TriggerEvent("mythic_progbar:client:progress", {
    name = "destroying",
    duration = config.harvestTime,
    label = "Destroying plant",
    useWhileDead = false,
    canCancel = true
  }, function(cancelled)
    isDestroying = nil
    TriggerServerEvent('ut_weed:doneDestroying', cancelled, id)
  end)
end)

AddEventHandler('ut_weed:fillItem', function(quality, type)
  TriggerServerEvent('ut_weed:fillItemServer', getClosestPlantInteraction(), quality, type)
end)

function getClosestPlantInteraction()
  -- local closest = nil
  ESX.TriggerServerCallback('ut_weed:getAllPlants', function(plant)
    local x = nil
    for k,v in pairs(activeInteractions) do
      local ped = GetPlayerPed(-1)
      local coords = GetEntityCoords(ped)
      if closest == nil and plant[k] ~= nil then
        closest = {
          id = k,
          dist = getDistance(coords, plant[k])
        }
      elseif plant[k] ~= nil then
        if getDistance(coords, plant[k]) <= closest.dist then
          closest = {
            id = k,
            dist = getDistance(coords, plant[k])
          }
        end
      end
    end
    return x
  end)
  Citizen.Wait(250)
  return closest
end